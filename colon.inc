%ifndef colon.inc
%define colon.inc
%macro colon 2

	%ifid %2
		%ifdef ptr_elem ;pointer to next element
			%%last: dq ptr_elem		; local label for list elem
		%else
			%%last: dq 0
		%endif
		%define ptr_elem  %%last
	%else
		%fatal 'second arg is not a label'
	%endif

	%ifstr %1
		db %1, 0					; Запись ключа(0-нуль терминированная строка)
		%2:
	%else
		%fatal 'first arg is not a string'
	%endif

%endmacro
%endif
