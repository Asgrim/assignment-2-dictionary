section .text
global print_err
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
; Принимает код возврата и завершает текущий процесс
exit:
    mov     rax, 60          ; 'exit' syscall number
	xor     rdi, rdi
	syscall
; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
    .loop:
	  	cmp byte [rdi + rax], 0
		je .end
		inc rax
	    jmp .loop
	.end:
		ret

print_err:
	 push rdi
     call string_length
	 mov rdx, rax
	 pop rsi
	 mov rdi, 2
	 mov rax, 1
	 syscall
     ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	 push rdi
     call string_length
	 mov rdx, rax
	 pop rsi
	 mov rdi, 1
	 mov rax, 1
	 syscall
     ret
; Принимает код символа и выводит его в stdout
print_char:
    push di
    mov rsi,rsp
	mov rdx, 1
	mov rdi, 1
	mov rax, 1
	syscall
	pop dx
	ret
; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov di, 10
	jmp print_char
  ; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov r9, rsp
	dec rsp
	mov byte [rsp], 0x0
	xor r8, r8
	xor rax, rax
	mov rax, rdi
	mov r8,10
	.loop:
		xor rdx, rdx
		div r8
		add rdx,48
		dec rsp
		mov [rsp], dl
		test rax, rax
		jz .end
		jmp .loop
	.end:
		mov rdi, rsp
		call print_string
		mov rsp, r9
		ret
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	test rdi, rdi
	jns print_uint
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi
	jmp print_uint
; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor rdx, rdx
	xor rax, rax
	xor r9,r9
	xor r8,r8
	.loop:
		mov r8b, byte[rdi + rdx]
		mov r9b, byte[rsi + rdx]
		cmp r8b, r9b
		jne .end
		add r8b, r9b
		cmp r8b, 0
		jz .true
		inc rdx
		jmp .loop
	.true:
		inc rax
	.end:
		ret
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	push 0
	xor rax, rax
	xor rdi, rdi
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop rax
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
 	xor rcx, rcx ;counter
	xor rax, rax
	push rdi
	mov r9, rdi ;buf pointer
	mov r8, rsi ;save size_t
	.loop:
		push rcx
		call read_char
		pop rcx
		cmp rax, 0xA
		je .b
		cmp rax, 0x20
		je .b
		cmp rax, 0x9
		je .b
		cmp rax, 0
		je .end
		cmp rcx, r8 
		jnl .E
		mov byte[r9 + rcx], al
		inc rcx
		jmp .loop
	.b:
		cmp rcx, 0
		je .loop
	.end:
		mov byte[r9+rcx],0
		pop rax
		mov rdx, rcx 
		ret
	.E:	
		pop rax
		xor rax, rax
		ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
  xor rsi, rsi ; buf
	xor rax, rax
	xor rdx, rdx ;lenght
	xor r8, r8 ;string counter
	xor r9, r9 ;char
	.loop:
		mov r9b, byte[rdi + r8]
		cmp r9b,0
		je .end
		cmp r9b, 58
		jnl .el
		cmp r9b, 48
		jl .el
		sub r9b, 48
		mov rsi, rax
		shl rax,3
		add rax, rsi
		add rax, rsi
		add rax, r9
		inc rdx
		jmp .l
		.el:
			cmp rdx, 0
			jne .end
			.l:
				inc r8
				jmp .loop			
	.end:
		ret	


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
  xor rcx, rcx ;last not number symbol
	xor rsi, rsi ; buf
	xor rax, rax
	xor rdx, rdx ;lenght
	xor r8, r8 ;string counter
	xor r9, r9 ;char
	.loop:
		mov r9b, byte[rdi + r8]
		cmp r9b,0
		je .end
		cmp r9b, 58
		jnl .el
		cmp r9b, 48
		jl .el
		sub r9b, 48
		mov rsi, rax
		shl rax,3
		add rax, rsi
		add rax, rsi
		add rax, r9
		inc rdx
		jmp .l
		.el:
			cmp rdx, 0
			jne .end
			mov rcx, r9
			.l:
				inc r8
				jmp .loop			
	.end:
		cmp rcx,45
		je .ni
		ret	
	.ni:
		inc rdx
		neg rax
		ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	push rdi
	push rsi
	push rdx
    call string_length
	pop rdx
	pop rsi
	pop rdi
    cmp rdx, rax
    jbe .e
    push rax
	.loop: 
	    mov dl, byte[rdi]
	    mov byte[rsi], dl
	    inc rdi
	    inc rsi
	    cmp dl, 0
	    jnz .loop 
	    pop rax 
	    ret
	.e:
	    xor rax, rax
	    ret
