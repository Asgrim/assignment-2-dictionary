global find_word
extern string_equals
section .text

find_word:
	xor r8, r8
	.loop:
		push rdi
		push rsi
		add rsi, 8
		call string_equals
		pop rsi
		pop rdi  
		cmp rax, 1
		je .find
		mov qword r8, [rsi] ;read addr to next elem
    	cmp r8, 0
		jz .not_found
		mov rsi, r8
		jmp .loop
	.find:
		mov rax, rsi
		ret  
	.not_found:
		xor rax, rax
		ret
