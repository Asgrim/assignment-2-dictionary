%define BUF_SIZE 255
%include "lib.inc"

extern find_word
section .rodata
err_mes_no_key: db "error: no such key",0 
err_mes_str_size: db "error: string is too long, max is 255 symbols",0

section .bss 
buf: resb BUF_SIZE

section .data
%include "words.inc"
section .text
global _start

_start:
	mov rdi, buf
	mov rsi, BUF_SIZE
	call read_word
	cmp rax,0
	je .err_str_size
	mov rdi, buf
	mov rsi, ptr_elem ;pointer to first elem of dict
	call find_word
	cmp rax, 0
	je .err_no_key
	mov rdi, rax
	add rdi, 8
	push rdi
	call string_length
	pop rdi
	add rax, rdi 
	inc rax
	mov rdi, rax
	call print_string
	call print_newline
	xor rdi, rdi
	call exit
	
	.err_no_key:
		mov rdi, err_mes_no_key
		call print_err
		call print_newline
		mov rdi,1 
		call exit
	.err_str_size:
		mov rdi, err_mes_str_size
		call print_err
		call print_newline
		mov rdi,1 
		call exit
