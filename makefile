main: main.o lib.o dict.o 
	ld -o main lib.o main.o dict.o
	rm *.o
%.o: %.asm
	nasm -f elf64 -o $@ $<
.PHONY: clean
clean:
	rm *.o
                   
